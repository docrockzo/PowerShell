Stop-Process -ProcessName explorer -Force

Write-Verbose "`nCombine taskbar buttons - "Never""
Set-ItemProperty HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced\ -Name TaskbarGlomLevel -Value 2 -Verbose

Write-Verbose "`nShow file extensions"
Set-ItemProperty HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced\ -Name HideFileExt -Value 0 -Verbose

Write-Verbose "`nShow hidden files"
Set-ItemProperty HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced\ -Name Hidden -Value 1 -Verbose

Write-Verbose "`nRemove taskbar search box"
New-ItemProperty HKCU:\Software\Microsoft\Windows\CurrentVersion\Search\ -Name SearchboxTaskbarMode -Value 0 -PropertyType DWORD -Verbose

Start-Process "explorer.exe" -Verbose

Write-Verbose "Complete"
Start-Sleep -Seconds 5