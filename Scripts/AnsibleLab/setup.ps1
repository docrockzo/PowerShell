# Script for template VMs in a lab environment
# Launched by: StartConf.ps1

# Validate user is an Administrator
Write-Host "Checking Administrator credentials" -ForegroundColor Green
If (-NOT ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole(`
    [Security.Principal.WindowsBuiltInRole] "Administrator")) {
    Write-Warning "You are not running this as an Administrator!`nRe-running script and will prompt for administrator credentials."
    Start-Process -Verb "Runas" -File PowerShell.exe -Argument "-STA -noprofile -file $($myinvocation.mycommand.definition)"
    Break
}

#region network adapter configuration
# If DHCP is enabled on adapter, manually set adapter information & disable IPv6
$netAdapter = Get-NetAdapter
$netIpInt = Get-NetIPInterface | Where-Object {$_.AddressFamily -eq "IPv4" -and $_.InterfaceAlias -notlike "Loopback*"}

if ((Get-NetIPInterface -InterfaceAlias $netIpInt.InterfaceAlias -AddressFamily IPv4).Dhcp -eq "Enabled")
{
        $ip = Read-Host "New IP address"
        $maskBits = 24  # 255.255.255.0
        $gateway = "GW_CHANGE_ME"
        $dnsAddress = "DNS01_CHANGE_ME","DNS02_CHANGE_ME"

        # Remove current adapter settings
        $netAdapter | Remove-NetIPAddress -Confirm:$false -Verbose
        $netAdapter | Remove-NetRoute -Confirm:$false -Verbose
        
        # Test if desired IP is in use
        $testNetCon = Test-NetConnection $ip -Verbose
        if ($testNetCon.PingSucceeded -eq "True")
        {
            Write-Warning "IP address conflict" -Verbose
            #TODO: loop back to $ip
        }
        else
        {
            # Set IP, subnet mask, gateway, DNS; Disable IPv6
            $netAdapter | New-NetIPAddress -AddressFamily IPv4 -IPAddress $ip -PrefixLength $maskBits -DefaultGateway $gateway
            Set-DnsClientServerAddress $netAdapter.Name -ServerAddresses ($dnsAddress[0],$dnsAddress[1]) -Verbose
            Disable-NetAdapterBinding -Name "*" -ComponentID ms_tcpip6 -ErrorAction SilentlyContinue -Verbose
        }
}
#endregion

#region computer configuration
# Create a new admin account
$userName = Read-Host "`nNew local account username"
$userPassword = Read-Host "Password" -AsSecureString

$params = @{
    Name        = $userName
    Password    = $userPassword
    FullName    = $userName
}

New-LocalUser @params -AccountNeverExpires -PasswordNeverExpires -Confirm:$false -Verbose
Add-LocalGroupMember -Group "Administrators" -Member $userName -Verbose

# Join domain
Write-Host "`nJoining domain..." -ForegroundColor Green
Start-sleep -Seconds 5
$domainCred = Get-Credential
Add-Computer -DomainName "anslab.xyz" -OUPath "OU=Servers,DC=anslab,DC=xyz" -Credential $domainCred -Verbose

# Register DNS
ipconfig /registerdns

# Allow all inbound traffic
Write-Host "`nAllowing all inbound traffic..." -ForegroundColor Green
Set-NetFirewallProfile -All -DefaultInboundAction Allow


Write-Host "`nRemoving setup task..." -ForegroundColor Green
Unregister-ScheduledTask -TaskName "Server Setup" -Confirm:$false -Verbose

Write-Host "`nRestarting..." -ForegroundColor Green
Start-Sleep -Seconds 5
Restart-Computer -Verbose