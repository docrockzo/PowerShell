# Validate user is an Administrator
Write-Host "Checking Administrator credentials" -ForegroundColor Red
If (-NOT ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole(`
    [Security.Principal.WindowsBuiltInRole] "Administrator")) {
    Write-Warning "You are not running this as an Administrator!`nRe-running script and will prompt for administrator credentials."
    Start-Process -Verb "Runas" -File PowerShell.exe -Argument "-STA -noprofile -file $($myinvocation.mycommand.definition)"
    Break
}

# Update computer name
$newCN = Read-Host "`nEnter new computer name"
Rename-Computer -NewName $newCN -Verbose

# Scheduled task to resume setup
$action = New-ScheduledTaskAction -Execute "PowerShell.exe" -Argument "C:\Scripts\setup.ps1 -NoProfile -ExecutionPolicy Bypass"
$trigger = New-ScheduledTaskTrigger -AtLogOn
$principal = New-ScheduledTaskPrincipal -GroupID "BUILTIN\Administrators" -RunLevel Highest
$settings = New-ScheduledTaskSettingsSet
New-ScheduledTask -Action $action -Principal $principal -Trigger $trigger -Settings $settings
Register-ScheduledTask -Action $action -TaskName "Server Setup" -Trigger $trigger

Write-Host "`nRebooting..." -ForegroundColor Green
Start-sleep -Seconds 5
Restart-Computer -Verbose