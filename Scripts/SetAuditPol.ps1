#Requires -RunAsAdministrator

Write-Host "Setting advanced audit policies...`n"

auditpol.exe /set /subcategory:"Security System Extension" /success:enable
auditpol.exe /set /subcategory:"System Integrity" /success:enable /failure:enable
auditpol.exe /set /subcategory:"IPsec Driver" /success:enable /failure:enable
auditpol.exe /set /subcategory:"Other System Events" /success:enable /failure:enable
auditpol.exe /set /subcategory:"Security State Change" /success:enable
auditpol.exe /set /subcategory:"Logon" /success:enable /failure:enable
auditpol.exe /set /subcategory:"Logoff" /success:enable
auditpol.exe /set /subcategory:"Account Lockout" /failure:enable
auditpol.exe /set /subcategory:"Special Logon" /success:enable
auditpol.exe /set /subcategory:"Group Membership" /success:enable
auditpol.exe /set /subcategory:"Other Object Access Events" /success:enable /failure:enable
auditpol.exe /set /subcategory:"Removable Storage" /success:enable /failure:enable
auditpol.exe /set /subcategory:"Sensitive Privilege Use" /success:enable /failure:enable
auditpol.exe /set /subcategory:"Process Creation" /success:enable
auditpol.exe /set /subcategory:"Plug and Play Events" /success:enable
auditpol.exe /set /subcategory:"Audit Policy Change" /success:enable /failure:enable
auditpol.exe /set /subcategory:"Authentication Policy Change" /success:enable
auditpol.exe /set /subcategory:"Authorization Policy Change" /success:enable
auditpol.exe /set /subcategory:"Security Group Management" /success:enable
auditpol.exe /set /subcategory:"Other Account Management Events" /success:enable
auditpol.exe /set /subcategory:"User Account Management" /success:enable /failure:enable
auditpol.exe /set /subcategory:"Credential Validation" /success:enable /failure:enable

Write-Host "`nPress any key to continue..."
$null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown')